<?php

namespace App\Models;


/**
 * Type model
 *
 * PHP version 7.0
 */
class Type extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::db();
        
        $data = $db->query('
            SELECT 
                id,
                name
            FROM types;')->fetchAll();
        return $data;
    }
}
