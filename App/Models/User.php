<?php

namespace App\Models;


/**
 * User model
 *
 * PHP version 7.0
 */
class User extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::db();

        $data = $db->query('
            SELECT 
                id,
                name
            FROM users;')->fetchAll();
        return $data;
    }
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getUserBySession($session)
    {
        $db = static::db();

        $data = $db->query("
            SELECT 
                id,
                name
            FROM users
            WHERE session = '$session';")->fetchAll();
        return $data;
    }
}
