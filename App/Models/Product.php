<?php

namespace App\Models;

/**
 * Product model
 *
 * PHP version 7.0
 */
class Product extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::db();

        $data = $db->query('
            SELECT 
                id,
                name
            FROM products')->fetchAll();
        return $data;
    }
}
