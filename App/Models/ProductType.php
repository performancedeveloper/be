<?php

namespace App\Models;


/**
 * Product Type Model
 *
 * PHP version 7.0
 */
class ProductType extends \Core\Model {

    /**
     * Get all the product types as an associative array
     *
     * @return array
     */
    public static function getAll() {
        $db = static::db();

        $data = $db->query('
            SELECT 
                p.id AS product_id, 
                t.id AS type_id, 
                p.name AS product_name, 
                t.name AS type_name, 
                c.value,
                t.description AS type_description
            FROM product_type_values c
            INNER JOIN products p ON (p.id = c.product_id)
            INNER JOIN types t ON (t.id = c.type_id)')->fetchAll();
        
        return $data;
    }

    /**
     * Get all products by type_id as an associative array
     *
     * @return array
     */
    public static function getBy($type_id) {
        $db = static::db();
        $data = $db->query('
            SELECT 
                p.id as product_id, 
                t.id as type_id, 
                p.name as product_name, 
                t.name as type_name, 
                c.value,
                t.description AS type_description
            FROM product_type_values c
            INNER JOIN products p ON (p.id = c.product_id)
            INNER JOIN types t ON (t.id = c.type_id)
            where t.id = \'' . $type_id . '\'')->fetchAll();
        return $data;
    }

}
