<?php

namespace App\Controllers;

use \Core\View;

/**
 * Products controller
 *
 * PHP version 7.0
 */
class Products extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $products = \App\Models\Product::getAll();
        
        View::renderTemplate('Products/index.html', $products);
    }
    
    /**
     * Show the index page
     *
     * @return void
     */
    public function storeAction()
    {
        $data = array();
        
        View::renderTemplate('Products/index.html', $data);
    }
}
