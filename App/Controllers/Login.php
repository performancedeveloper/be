<?php

namespace App\Controllers;

use \Core\View;

/**
 * Login controller
 *
 * PHP version 7.0
 */
class Login extends \Core\Controller
{

    /**
     * Show the login index page
     *
     * @return void
     */
    public function indexAction()
    {
        $user = null;
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
            $sh1 = sha1($_POST['name'] . $_POST['password']);
            
            $user = \App\Models\User::getUserBySession($sh1);
            
            if(!empty($user)){
                
                setcookie('uid', $sh1);
                header('Location: /');
            }
        }
        
        View::renderTemplate('Login/index.html', array('user' => $user));
        
    }
}
