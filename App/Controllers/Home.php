<?php

namespace App\Controllers;

use \Core\View;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Home extends \Core\Controller {

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction() {

        $sh1 = @$_COOKIE['uid'];

        $user = \App\Models\User::getUserBySession($sh1);

        if (!empty($user)) {
            $data['types'] = \App\Models\Type::getAll();
            $data['products'] = \App\Models\Product::getAll();

            $id = (isset($this->route_params['type_id']) && is_numeric($this->route_params['type_id'])) ? $this->route_params['type_id'] : 0;

            $data['productTypes'] = \App\Models\ProductType::getBy($id);


            View::renderTemplate('Home/index.html', $data);
        }
    }

    /**
     * Check is user logedin
     *
     * @return void
     */
    public function before() {
        $sh1 = @$_COOKIE['uid'];

        $user = \App\Models\User::getUserBySession($sh1);

        if (empty($user)) {
            $ctr = new \App\Controllers\Login('');
            $ctr->indexAction();
        }
    }

}
