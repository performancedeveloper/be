<?php

namespace App;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{

    /**
     * Database type
     * @var string
     */
    static $DB_TYPE = null;
    
    /**
     * Database host
     * @var string
     */
    static $DB_HOST = null;

    /**
     * Database name
     * @var string
     */
    static $DB_NAME = null;

    /**
     * Database user
     * @var string
     */
    static $DB_USER = null;

    /**
     * Database password
     * @var string
     */
    static $DB_PASSWORD = null;

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    static $SHOW_ERRORS = true;
    
    public static function db(){
        
        try{
        
            
            $db_config = Yaml::parseFile(__DIR__ . '/db_config.yaml');
            
            foreach($db_config['parameters'] as $key => $param){
                
                if(!( self::$$key)){
                    self::$$key = $param;
                }
            }
            
        } catch (ParseException $exception) {
            
            var_dump($e);
            exit;
        }
    }
}
