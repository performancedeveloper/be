# README #

PHP Developer Test

### What is this repository for? ###

Create a 3-step quotation process that has the following steps:
Let the user enter their details as listed below, and once completed they should advance on to the next step.
Name
Password
Email Address
Phone Number
Products to quote for, there are three different types of product, all that have pricing based on different factors. 
They are outlined below, and an example of each provided, feel free to set up with just these three products.

### How do I get set up? ###

- Enable apache mod_rewrite > a2enmod rewrite
- Run composer update to install the project dependencies.
- Configure your web server to have the *** html *** folder as the web root.

*** Database: *** 


- Edit and set parameters in db_config.yml

*** Using docker composer ***

- Edit docker-compose.yml
- Run docker-compose up

*** User for test: ***

- Name: test
- Password: test

