<?php

namespace Core;

use Medoo\Medoo;
use App\Config;

/**
 * Base model
 *
 * PHP version 7.0
 */
abstract class Model {

    static protected $database = null;

    /**
     * Get the PDO database connection
     *
     * @return mixed
     */
    protected static function db() {

        // Set config params
        Config::db();
        
        // Set connection
        if (self::$database === null) {
            
            self::$database = new Medoo([
                'database_type' => Config::$DB_TYPE,
                'database_name' => Config::$DB_NAME,
                'server' => Config::$DB_HOST,
                'username' => Config::$DB_USER,
                'password' => Config::$DB_PASSWORD
            ]);
        }
        
        return self::$database;
    }

}
