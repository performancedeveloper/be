# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.2.14-MariaDB-10.2.14+maria~jessie)
# Database: products
# Generation Time: 2018-08-16 16:39:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table product_type_values
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_type_values`;

CREATE TABLE `product_type_values` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `product_type_values` WRITE;
/*!40000 ALTER TABLE `product_type_values` DISABLE KEYS */;

INSERT INTO `product_type_values` (`id`, `product_id`, `type_id`, `value`)
VALUES
	(1,1,1,10),
	(2,1,2,50),
	(3,1,3,80),
	(4,2,1,70),
	(5,2,2,60),
	(6,4,3,35);

/*!40000 ALTER TABLE `product_type_values` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`)
VALUES
	(1,'English lessons I'),
	(2,'English lessons II'),
	(3,'English lessons III'),
	(4,'Alice in wonderland');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;

INSERT INTO `types` (`id`, `name`, `description`)
VALUES
	(1,'subscription','per hour'),
	(2,'service','per day'),
	(3,'goods','lifelong');

/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_product_values
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_product_values`;

CREATE TABLE `user_product_values` (
  `user_id` int(11) NOT NULL,
  `product_type_value_id` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `expired` enum('0','-1','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-1',
  `value` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`,`product_type_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_product_values` WRITE;
/*!40000 ALTER TABLE `user_product_values` DISABLE KEYS */;

INSERT INTO `user_product_values` (`user_id`, `product_type_value_id`, `updated`, `expired`, `value`)
VALUES
	(1,1,'2018-08-16 00:58:38','-1',20);

/*!40000 ALTER TABLE `user_product_values` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(23) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `session` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `password`, `email`, `phone`, `session`)
VALUES
	(1,'test','test','test@email.com','+3322211234','51abb9636078defbf888d8457a7c76f85c8f114c'),
	(2,'test2','test2','test2@email.com','+3422211234','272a11a0206b949355be4b0bda9a8918609f1ac6');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
